#include <stdlib.h>
#include <stdio.h>
#include <argp.h>
#include <syslog.h>
#include <iostream>
#include "AzertCli.h"
using namespace std;


int main(int compteur_arguments, char **tableau_arguments)
{
  openlog(NULL, LOG_CONS | LOG_PID, LOG_USER);
  closelog();
  AzertCli *ma_CLI = AzertCli::instance();
  ma_CLI->run(compteur_arguments, tableau_arguments);
  AzertyMusicLogo();
  ecritureFichier();
  return 0;
}

