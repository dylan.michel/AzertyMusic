#include <iostream>
#include <iomanip>
#include <limits>
#include <cstring>
#include <libpq-fe.h>
#include <syslog.h>
#include "connexion.h"


using namespace std;
std::string connexion(string temp_recherche, string user, string format)
{
  openlog(NULL, LOG_CONS | LOG_PID, LOG_USER);
  const char *connexion_infos;
  PGconn *connexion;
  string leresultats = "-1";
  string infoCon = "host='postgresql.bts-malraux72.net' port=5432 dbname='d.michel' user='" + user + "' connect_timeout=5";
  connexion_infos = infoCon.c_str();

  if(PQping(connexion_infos) == PQPING_OK)
  {
    // Établissement de la connexion avec les paramètres
    // fournis dans la chaîne de caractères 'connexion_infos'
    connexion = PQconnectdb(connexion_infos);

    if(PQstatus(connexion) == CONNECTION_OK)
    {
      // Récupération des données
      syslog(LOG_INFO, "Connection succes to postgreSQL");
      const char *requete_sql = temp_recherche.c_str();
      PGresult *resultats = PQexec(connexion, requete_sql);
      ExecStatusType etat_execution = PQresultStatus(resultats);

      // Vérification de l'état des données reçues
      if(etat_execution == PGRES_TUPLES_OK)
      {
        // Exploitation des résultats
        if(format == "M3U")
        {
          leresultats = "";

          for(unsigned int nb_donnee = 0; (int)nb_donnee < PQntuples(resultats); nb_donnee++)
          {
            leresultats += PQgetvalue(resultats, nb_donnee, 0) ;
            leresultats += "\n";
          }
        }
        else
        {
          for(unsigned int nb_donnee = 0; (int)nb_donnee < PQntuples(resultats); nb_donnee++)
          {
            leresultats += "    <track>\n      <location>file://";
            leresultats += PQgetvalue(resultats, nb_donnee, 0) ;
            leresultats += "</location>\n    </track>\n";
          }
        }
      }
      else if(etat_execution == PGRES_EMPTY_QUERY)
      {
        std::cerr << "La chaîne envoyée au serveur était vide." << endl;
        syslog(LOG_WARNING, "the string sent to the server was empty");
      }
      else if(etat_execution == PGRES_COMMAND_OK)
      {
        std::cerr << "La requête SQL n'a renvoyé aucune donnée." << std::endl;
        syslog(LOG_WARNING, "The SQL query did not return any data.");
      }
      else if(etat_execution == PGRES_COPY_OUT)
      {
        std::cerr << "Début de l'envoi (à partir du serveur) d'un flux de données." << std::endl;
        syslog(LOG_INFO, "Start sending (from the server) a data stream.");
      }
      else if(etat_execution == PGRES_COPY_IN)
      {
        std::cerr << "Début de la réception (sur le serveur) d'un flux de données." << std::endl;
        syslog(LOG_INFO, "Start of the reception (on the server) of a data flow.");
      }
      else if(etat_execution == PGRES_BAD_RESPONSE)
      {
        std::cerr << "La réponse du serveur n'a pas été comprise." << std::endl;
        syslog(LOG_WARNING, "The server response was not understood");
      }
      else if(etat_execution == PGRES_NONFATAL_ERROR)
      {
        std::cerr << "Une erreur non fatale (une note ou un avertissement) est survenue." << std::endl;
        syslog(LOG_WARNING, "A unfatal error (a note or warning) has occurred.");
      }
      else if(etat_execution == PGRES_FATAL_ERROR)
      {
        std::cerr << "Une erreur fatale est survenue." << std::endl;
        syslog(LOG_CRIT, "A fatal error has occurred.");
      }
      else if(etat_execution == PGRES_COPY_BOTH)
      {
        std::cerr << "Lancement du transfert de données Copy In/Out (vers et à partir du serveur)." << std::endl;
        syslog(LOG_INFO, "Starting Copy In / Out data transfer (to and from the server).");
      }
      else if(etat_execution == PGRES_SINGLE_TUPLE)
      {
        std::cerr << "La structure PGresult contient une seule ligne de résultat provenant de la commande courante." << std::endl;
        syslog(LOG_INFO, "The PGresult struct contains a single result line from the current command.");
      }

      PQclear(resultats);
    }
    else
    {
      std::cerr << "Malheureusement la connexion n'a pas pu être établie" << std::endl;
      syslog(LOG_CRIT, "the connection could not be established");
    }

    PQfinish(connexion);
  }
  else
  {
    std::cerr << "Malheureusement le serveur n'est pas joignable. Vérifier la connectivité" << std::endl;
    syslog(LOG_CRIT, "the server is not reachable. Check connectivity");
  }

  closelog();
  return leresultats;
}
