/*!
 \file AzertCli.h
 \brief Playlist base program
 \author MICHEL Dylan, JURET Enzo
 \version 1.0
 \date 13 december 2018
 Define of the playlist class with sub class to make the request sql and error_t

 */



#ifndef AZERTCLI_H
#define AZERTCLI_H
#include <cstring>
#include <iostream>
#include <list>
#include <argp.h>
#include <fstream>

using namespace std;

/*!
  Abstract class to make choose between genre, artist, album, title
  \class AzertCCM
 */
class AzertCCM
{
 protected:
  //! Protected variables
  /*!
    It going to contain the arg for -A, -a, -g or -t
   */
  string leString;
  //! Protected variables
  /*!
    It going to contain the percent for arg -A, -a, -g or -t
   */
  int percent;
 public:

  //! Public method
  /*!
    Method who allow to get Percent
    \return "percent"
   */
  int getPercent();

  //! Public method
  /*!
    Method who allow to get leString
    \return leString
   */
  string getleString();

  //! Public method
  /*!
    Method who allow to define the percent for this choose
    Méthode qui permet de définir le poucentage de se choix
    \param _percent Value use as percent to define the duration with AzertCli->duree
   */
  void setPercent(int _percent);

  //! Public method
  /*!
    Method who allow to define the string of this choose
    Méthode qui permet de définir le string
    \param _leString Value use to define the search in database
   */
  void setleString(string _leString);

  //! Constructor
  /*!
    Constructor of AzertCCM
    \param search
    \param perc
   */
  AzertCCM(string search, int perc);

  //! Pure virtual method
  /**!
     Mother method, which going to return the sql request for the other subclass
   */
  virtual string requeteSql() = 0;
};

//!Sub class of AzertCCM
/*!
 From abstact class AzertCCM, define genre
 class Genre:AzertCCM
 */
class Genre : public AzertCCM
{
 public:
  string requeteSql();

  //!Constructor of Genre::AzertCCM
  /*!
    /param search The values enter in the argparce
    /param perc The values enter in the subarg
  */
  Genre(string search, int perc);

};

//!Sub class of AzertCCM
/*!
 From abstact class AzertCCM, define artist
 class Artist:AzertCCM
 */
class Artist : public AzertCCM
{
  string requeteSql();
 public :

  //!Constructor of Artist::AzertCCM
  /*!
    /param search The values enter in the argparce
    /param perc The values enter in the subarg
  */
  Artist(string search, int perc);

};

//!Sub class of AzertCCM
/*!
 From abstact class AzertCCM, define album
 class Album:AzertCCM
 */
class Album : public AzertCCM
{
  string requeteSql();
 public :
  //!Constructor of Album::AzertCCM
  /*!
    /param search The values enter in the argparce
    /param perc The values enter in the subarg
  */
  Album(string search, int perc);

};

//!Sub class of AzertCCM
/*!
  From abstact class AzertCCM, define title
  class Title:AzertCCM
 */
class Title : public AzertCCM
{
  string requeteSql();
 public :

  //!Constructor of Title::AzertCCM
  /*!
    /param search The values enter in the argparce
    /param perc The values enter in the subarg
  */
  Title(string search, int perc);

};


//!Main class
/*!
  Main class who use AzertCCM and used to make the playlist
  class AzertCli
 */
class AzertCli
{
 protected:
  //! Protected variables
  /*!
  Use to contain the duration remaining as % for the playlist
  */
  int dureeRestante;

  //! Protected variables
  /*!
  Use to contain the duration as second of the playlist
  */
  int duree;


  //! Protected variables
  /*!
  Use to contain the boolean who decide if its M3U -> 0, or XSPF -> 1
  */
  bool format;

  //! Protected variables
  /*!
  Use to contain a boolean to generate new default describe if it doesn't change
  */
  bool descriUp;


  //! Protected variables
  /*!
  Use to contain the description of the playlist
  */
  string description;

  //! Protected variables
  /*!
  Use to contain the name of the playlist
  */
  string name;

  //! Protected variables
  /*!
  Use to contain the user of the database
  */
  string user;

 public:

  /*!
    Méthode d'utilisation du singleton
    Method to use the singleton
   */
  static AzertCli *instance();

  /*!
    Liste de AzertCCM
    List of AzertCCM
   */
  list <AzertCCM *> laList;

  /*!
    Méthode qui retourne le % de durée restante
    Method that return the % of duration remaining
   \return dureeRestante
   */
  int getDureeRestante();

  /*!
    Méthode qui retourne la durée totale en seconde de la playlist
    Method that return the duration as second of the playlist
   \return duree
   */
  int getDuree();

  /*!
    Méthode qui retourne le format en string, soit 0 M3U  soit 1 XSPF
    Method that return the format as string, 0 for M3U or 1 for XSPF
   \return format
   */
  string getFormat();

  /*!
    Méthode qui retourne la description de la playlist
    Method that return the describe of the playlist
   \return description
   */
  string getDescription();

  /*!
    Méthode qui retourne le nom de la playlist
    Method that return the name of the playlist
   \return name
   */
  string getName();

  /*!
    Méthode qui retourne l'utilisateur de la base de donnée
    Method that return the user of the database
   */
  string getUser();


  /*!
    Méthode qui retourne un boolean pour changé la durée de la description si elle n'est pas changé
    Method that return the boolean for change the base description duration if it not change
   \return descriup
   */
  bool getBoolDescri();

  /*!
    Méthode qui permet de mettre à jour le % restant de la playlist
    Method that allow to update the % left of the playlist
    \param _dureeRestante The value to define dureeRestante
   */
  void setDureeRestante(int _dureeRestante);

  /*!
    Méthode qui permet de definir la durée en seconde de la playlist
    Method that allow to set the duration of the playlist as second
    \param _duree The value to define duree
   */
  void setDuree(const char *_duree);

  /*!
    Méthode qui permet de définir le format de la playlist, 0 = M3U, 1=XSPF
    Method that allow to define the format of the playlist, 0 = M3U, 1=XSPF
    \param _format The values to set boolean choose 0 for M3U, 1 for XSPF. Set to 0 if you don't put 0 or 1
   */
  void setFormat(string _format);

  /*!
    Méthode qui permet de définir la description de la playlist
    Method that allow to define the describe of the playlist
    \param _description The value to set private variable description
   */
  void setDescription(string _description);

  /*!
    Méthode qui permet de définir le nom de la playlist
    Method that allow to define the name of the playlist
    \param _name The value to set private variable name
   */
  void setName(string _name);

  /*!
    Méthode qui permet de définir l'utiliseur de la base de données
    Method that allow to define the database user
    \param _user The value to set private variable user
   */
  void setUser(string _user);

  /*!
    Méthode qui permet de retourné chaque chemin des musiques soit en XSPF ou en M3U en fonction du format
    Method that allow to return every music path in XPSF or in M3U
   \return a string who contain all path in format M3U or XSPF
   */
  string Defilist();

  /*!
    Méthode qui retourne une requête supplementaire avec le pourcentage de durée restante
    Method that return an sqlrequest if the user didn't use all the dureerestante
    \param _leschemins Allow to writer over the initialised variable who contain the music path
   */
  void siDureePasVide(string &_leschemins);

  /*!
    "Méthode qui permet de définir le boolean description"
    "Method that allow to set the boolean of the describ, if the describ isn't change so it update the duration of the describe playlist"
    \param _descriUp "The value to set the boolean for the describe"
   */
  void setBoolDescriT(bool _descriUp);

  /*!
    "Méthode qui permet de definir les arguments possibles"
    "Method who allow to define the error_t"
    \return error_t
   */
  error_t run(int argc, char **argv);

  /*!
    "Méthode qui permet de passer les arguments"
    "Method who allow to put the args"
    \return int
   */
  static int parse_opt(int key, char *arg, struct argp_state *state);
 private:
  /*!
    "Constructeur par défauts de AzertCli"
    "Default constructor of AzertCli"
   */
  AzertCli();

  /*!
  "Declaration du singleton"
  "Declaration of the singleton"
   */
  static AzertCli *singleton;

};

/*!
  "Méthode d'affichage qui permet de retourner le logo en affichage console"
  "Method who allow to diplay the logo on the console"
 */
void AzertyMusicLogo();


/*!
  "Méthode qui permet d'écrire le fichier en M3U ou XSPF"
  "Method who allow to write the playlist, M3U or XSPF"
 */
void ecritureFichier();

/*!
  "Méthode qui permet de retourner un int, pour déterminer le % passer en sous option "
  "Method who allow to return as int, the % found after the '='"
  \param _argbeopt "Argument"
  \param afterEgale "Variable initialise by SUBwhereisegale"
 \return "a value found after the '='"
 */
int SUBmethodeSubOPTman(string _argbeopt, int afterEgale);


/*!
  "Méthode qui permet de retourner un int, soit l'emplacement du égale"
  "Method who allow to return an int, where the method found the '=' or -1 if not"
  \param _argbeopt "Arg pass on the parsing"
 \return "a value where it found the '='"
 */
int SUBwhereisEgale(string _argbeopt);


/*!
  "Méthode qui permet de retourner un string sans = du argp"
  "Method who allow to return the string without '='"
  \param _argbeopt "Arg put in the ARGP"
  \param whereisEgale "Variable initialise with SUBwhereisEgale"
 \return string
*/
string SUBsupprEgale(string _argbeopt, int whereisEgale);

#endif
