#include <iostream>
#include <string>
#include <fstream>
#include <list>

using namespace std;

int main()
{
  ofstream fichier("test.XSPF", ios::out | ios::trunc);
  string description_argu = "Voici la playlist de vos rêves en XSPF!";
  string hautdepage = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>";
  string version = "1";
  string versionXSPF = "http://xspf.org/ns/0/";

  if(fichier)
  {
    string description = "<!--" + description_argu + "-->";
    fichier << description << endl << hautdepage << endl;
    fichier << "<playlist version=\"" << version << "\"" << " xmlns=\"" << versionXSPF << "\">" << endl;
    fichier << "  <tracklist>" << endl;
    list<string> lalisteMusique;
    lalisteMusique.push_back("/music/Enzo.mp3");
    lalisteMusique.push_back("/music/Florent.mp3");
    lalisteMusique.push_back("/music/Alex.mp3");
    lalisteMusique.push_back("/music/Dylan.mp3");
    lalisteMusique.push_back("/music/Mathis.mp3");
    lalisteMusique.push_back("/music/Hugo.mp3");
    lalisteMusique.push_back("/music/Clement.mp3");
    lalisteMusique.push_back("/music/Victor.mp3");
    lalisteMusique.push_back("/music/Dilhan.mp3");

    for(string a : lalisteMusique)
    {
      fichier << "    <track>" << endl;
      fichier << "      <location>file://" << a << "</location>" << endl;
      fichier << "    </track>" << endl;
    }

    fichier << "  </tracklist>" << endl << "</playlist>";
    fichier.close();
  }
  else
  {
    cerr << "Impossible d'ouvrir le fichier !" << endl;
  }

  return 0;
}
