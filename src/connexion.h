#include <string>
/**
 * "Used to make the connexion with database"
 * \param temp_recherche "Use as element on the request sql"
 * \param user "user of the database"
 * \param format "use to define which element to use"
 **/
std::string connexion(std::string temp_recherche, std::string user, std::string format);
