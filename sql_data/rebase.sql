SET SCHEMA 'ppe3';

DROP TABLE IF EXISTS "album_morceau" CASCADE;
DROP TABLE IF EXISTS "artiste_morceau" CASCADE;
DROP TABLE IF EXISTS "polyphonie" CASCADE;
DROP TABLE IF EXISTS "sousgenre" CASCADE;
DROP TABLE IF EXISTS "format" CASCADE;
DROP TABLE IF EXISTS "album" CASCADE;
DROP TABLE IF EXISTS "artiste" CASCADE;
DROP TABLE IF EXISTS "genre" CASCADE;
DROP TABLE IF EXISTS "morceau" CASCADE;

CREATE TABLE "polyphonie"
(
  id serial,
  nombre SMALLINT,
  PRIMARY KEY (id)
);

CREATE TABLE "genre"
(
  id serial,
  type text,
  PRIMARY KEY (id)
);

CREATE TABLE "format"
(
  id serial,
  titule text,
  PRIMARY KEY (id)
);

CREATE TABLE "sousgenre"
(
  id serial,
  sous_type text,
  PRIMARY KEY (id)
);

CREATE TABLE "album"
(
  id serial,
  date DATE,
  nom text,
  PRIMARY KEY (id)
);

CREATE TABLE "artiste"
(
  id serial,
  nom text,
  PRIMARY KEY (id)
);

CREATE TABLE "morceau"
(
  id serial,
  duree INT NOT NULL,
  nom text,
  chemin text,
  fk_polyphonie INT,
  fk_genre INT,
  fk_sousgenre INT DEFAULT NULL,
  fk_format INT,
  PRIMARY KEY (id),
  CONSTRAINT "fk_pm_id" FOREIGN KEY (fk_polyphonie) REFERENCES "polyphonie"(id),
  CONSTRAINT "fk_gm_id" FOREIGN KEY (fk_genre) REFERENCES "genre"(id),
  CONSTRAINT "fk_sgm_id" FOREIGN KEY (fk_sousgenre) REFERENCES "sousgenre"(id),
  CONSTRAINT "fk_fm_id" FOREIGN KEY (fk_format) REFERENCES "format"(id)
);

CREATE TABLE "album_morceau"
(
  id_album INTEGER NOT NULL,
  id_morceau INTEGER NOT NULL,
  PRIMARY KEY (id_album, id_morceau),
  CONSTRAINT "fk_album_id" FOREIGN KEY (id_album) REFERENCES "album"(id),
  CONSTRAINT "fk_amorceau_id" FOREIGN KEY (id_morceau) REFERENCES "morceau"(id)
);

CREATE TABLE "artiste_morceau"
(
  id_artiste INTEGER NOT NULL,
  id_morceau INTEGER NOT NULL,
  PRIMARY KEY (id_artiste, id_morceau),
  CONSTRAINT "fk_compositeur_id" FOREIGN KEY (id_artiste) REFERENCES "artiste"(id),
  CONSTRAINT "fk_amorceau_id" FOREIGN KEY (id_morceau) REFERENCES "morceau"(id)
);

